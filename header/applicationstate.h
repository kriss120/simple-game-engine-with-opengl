#ifndef APPLICATIONSTATE_H_INCLUDED
#define APPLICATIONSTATE_H_INCLUDED

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

enum InternalState {
    ACTIVE,
    COMPLETE,
};

class ApplicationState {
    public:
        InternalState state;
        ApplicationState* nextState;

        ApplicationState() {
            this->state = ACTIVE;
            this->nextState = NULL;
        }

        virtual void UpdateState(GLFWwindow* window);
        virtual void HandleInput(GLFWwindow* window);
        virtual ApplicationState* NextState();
    private:

};


#endif // APPLICATIONSTATE_H_INCLUDED
