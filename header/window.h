#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

using namespace std;

class Window {
    public:
        int windowHeight, windowWidth;
        const char* windowTitle;
        GLFWwindow* window;

    Window(int windowWidth, int windowHeight, const char* windowTitle) {
            this->windowHeight = windowHeight;
            this->windowWidth = windowWidth;
            this->windowTitle = windowTitle;

            this->CreateWindow();
        }
    private:
        virtual void CreateWindow();
};

#endif // WINDOW_H_INCLUDED
