#include <header/stateengine.h>
#include <header/applicationstate.h>

void StateEngine::UpdateState(GLFWwindow* window) {
    this->currentState->UpdateState(window);
    if (this->currentState->state == COMPLETE)
        this->currentState = this->currentState->nextState;
}

void StateEngine::HandleInput(GLFWwindow* window) {
    this->currentState->HandleInput(window);
}

