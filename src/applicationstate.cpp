#include <header/applicationstate.h>
#include <header/teststate.h>

void ApplicationState::UpdateState(GLFWwindow* window) {
    //Update render buffer
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    //Swap render buffer
    glfwSwapBuffers(window);
    glfwPollEvents();
}

ApplicationState* ApplicationState::NextState() {
    return this->nextState;
}

void ApplicationState::HandleInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        this->nextState = new TestState();
        this->state = COMPLETE;
        }
}
