#include <header/teststate.h>

void TestState::UpdateState(GLFWwindow* window) {
    //Update render buffer
    glClearColor(0.2f, 0.3f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    //Swap render buffer
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void TestState::HandleInput(GLFWwindow* window) {

}

ApplicationState* TestState::NextState() {

}


